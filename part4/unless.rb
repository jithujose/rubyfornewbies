engine_on = false

def service_engine
    "servicing engine!"
end

unless engine_on
    puts service_engine
else
    puts "engine is on, so rev up!"
end

# unless is equivalent of using if !

# equivalently
puts service_engine unless engine_on