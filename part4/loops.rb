# while loop
arr = ["John", "Jose", "Bla", "blu"]
i = 0
while arr[i] do
    puts arr[i]
    i += 1
end

# another way
puts "------------"
i = -1
puts arr[i] while arr[i+= 1]

# until loop
days_left = 7;
puts "------------\n"
until days_left == 0
    puts "there are still #{days_left} days left in the week"
    days_left -= 1
end

puts "------------\n"
x = 4
puts "hello #{x -= 1}" until x == 0

puts "------------\n"
x = 4
begin
    puts "hello #{x - 1}"
    x -= 1
end until x == 0


# for in loop
puts "------------\n"

for name in arr
    puts name
end

# for in using hashes

hash = {:name=>"Jithu", :age=>23, :job=>"student"}
for key, val in hash
    puts "#{key} is #{val}"
end