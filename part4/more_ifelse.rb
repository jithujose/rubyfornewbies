order = { :size => "medium" }

def make_medium_coffee
    puts "making medium coffee"
end

if order[:size] == "small"
    # No error will be thrown, if we don't enter here
    make_small_coffee;
    
elsif order[:size] == "medium"
    make_medium_coffee;
end

 
message = if order[:size] == "small"
    "make_small_coffee"
elsif order[:size] == "medium"
    "make_medium_coffee"
else
    "make_coffee" 
end

puts message
   
message = "making a medium coffee" if order[:size] == "medium"
puts message
