hour = 19

# We can break expressions using the ; or then keyword

message = case
        when hour < 12 then "Good morning!"
        when hour > 12 && hour < 17 then "Good afternoon!"
        else "Good evening!"
        end

puts message

# -- Equivalent way

message = case
        when hour < 12; "Good morning!"
        when hour > 12 && hour < 17; "Good afternoon!"
        else "Good evening!"
        end

puts message