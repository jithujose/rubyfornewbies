# switch cases in ruby

hour = 19

# case
# when hour < 12
#     puts "Good morning!"
# when hour > 12 && hour < 17
#     puts "Good afternoon!"
# else
#     puts "Good evening!"
# end


message = case
when hour < 12
    "Good morning!"
when hour > 12 && hour < 17
    "Good afternoon!"
else
    "Good evening!"
end

puts message