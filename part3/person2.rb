class Person2
    # Properties of this class
	attr_accessor :name, :age, :job
	attr_reader :sh_reader
	attr_writer :sh_writer
	
	# !!!
	# Class methods and varibles are not accessible by the
	# instances of this class
	
	# Class variable
	@@count = 0
    
    def initialize(name, age, job='unemployed')
        @name = name
        @age = age
        @job = job
        @sh_reader = "Shit"
        @@count += 1
    end
    
    # Class methods
    def self.count
        @@count
    end
    
	def greet
		"Hi #{@name}"
	end
	
	private
	# Everything below this is private 
    def get_job # private
        @job
    end
     
    def get_name # private
        @name
    end

end

