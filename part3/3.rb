puts "Blank objects\n"

o = Object.new
def o.my_method
  1 + 1
end

puts o.my_method

# --------------------------------

puts "Instance variables\n"

def o.set_name(name)
  @name = name
end

def o.get_name
  @name
end

o.set_name "Jithu"
puts o.get_name

# --------------------------------