class Person1

	# name: property of this class

	# Set method
	def name=(name)
		@name = name
	end

	# Get method
	def name
		@name
	end

end


# -----------------------
# It's possible to not write for each property the above two methods,
# by using attr_accessor
# -----------------------